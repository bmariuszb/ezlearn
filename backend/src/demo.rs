use actix_files::NamedFile;
use actix_web::{get, Responder};

#[get("/showcase/rubiks-cube")]
pub async fn rubiks_cube() -> impl Responder {
    NamedFile::open_async("./static/rubiks-cube-web/index.html").await
}
