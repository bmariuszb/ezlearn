mod demo;

use actix_files::{self as fs, NamedFile};
use actix_web::{get, web::to, App, HttpServer, Responder};

async fn index() -> impl Responder {
    NamedFile::open_async("./static/ezlearn-ui/index.html").await
}

#[get("/favicon.ico")]
async fn favicon() -> impl Responder {
    NamedFile::open_async("./static/ezlearn-ui/favicon.ico").await
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .default_service(to(index))
            .service(favicon)
            .service(fs::Files::new("/static", "static"))
            .service(demo::rubiks_cube)
    })
    .bind(("0.0.0.0", 3000))?
    .run()
    .await
}
