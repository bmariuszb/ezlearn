#!/bin/bash
# first source commands.sh
# update-static - update all files in static dir
# update-image - update docker image, do it after update-static

update-static() {
	echo "downloading latest ezlearn-ui"
	echo "downloading latest rubiks-cube-web"
}

update-image() {
#docker build -t rustacean557/ezlearn-backend:0.1.0 .
#docker push rustacean557/ezlearn-backend:0.1.0
#docker tag rustacean557/ezlearn-backend:0.1.0 rustacean557/ezlearn-backend:latest
#docker push rustacean557/ezlearn-backend:latest
	echo "login to docker"
	echo "building new image"
	echo "pushing new image"
}
